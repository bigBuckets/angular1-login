var User = require('./models/User');
var jwt = require('jsonwebtoken');
var jwtConfig = require('./jwt_config');

module.exports = function(app) {

	app.get('/test', function(req, res) {
		console.log('get test')

		console.log(req.headers)

		res.json({data: 'done test'})
	})
	app.post('/user/signup', function(req, res) {
		console.log(req.body)
		const { email, password } = req.body;

		User.create({ email: email, password: password })
			.then(function(user) {
				res.json(user)
		})
	})

	app.post('/user/login', function(req, res) {
		console.log(req.body)
		const { email, password } = req.body;

		User.findOne({email: email}).then(function(user) {
			console.log('--user--', user)
			if (user) {
				// Compare incoming password to db password
				if (user.password === password) {
					console.log('right password')
					const token = jwt.sign({
                        id: user._id,
                        email: user.email
                    }, jwtConfig.jwtSecret, {
                        expiresIn: 60 * 60 * 24 // expires in 24 hours
                    });
					res.json({token});
				}
				else {
					console.log('wrong password');
					res.json({
                        error: 'Sorry, the password does not match the username.'
                    })
				}
        	}
			else {
				console.log('no user found');
				res.json({ error: "Sorry, we don't recognize this username." });
			}
		})
		
	})

	app.get('*', function(req, res) {
		res.sendfile('./public/index.html');
	});


};