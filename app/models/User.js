var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var userSchema = new Schema({
	email: { type: String, required: true },
	password: { type: String, required: true },
	created_at: {type: Date, default: new Date()}
})

var User = mongoose.model("User", userSchema);

module.exports = User;