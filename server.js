var express        = require('express');
var app            = express();
var bodyParser     = require('body-parser');

var db = require('./config/db');
var port = process.env.PORT || 8080;

app.use(express.static(__dirname + '/bower_components'));
app.use(express.static(__dirname + '/public'));
app.use(bodyParser.json()); 
app.use(bodyParser.urlencoded({ extended: true }));

require('./app/routes')(app);

app.listen(port, function() {
    console.log('Listening on PORT >>> ' + port); 
});
	
exports = module.exports = app;