app.factory('userSvc', ['$http', '$ngRedux', 'jwtHelper', '$state', function($http, $ngRedux, jwtHelper, $state) {

	var self = this;
	// retrieve redux store
	this.mapStateToThis = function(state) {
		return {
			user: state.user
		}
	}
	$ngRedux.connect(this.mapStateToThis, { setCurrentUser })(this)	


	var service = {};
	service.signUp = function(email, password) {
		return $http.post('/user/signup', {email: email, password: password})
    }

	service.login = function(email, password) {
		
		return $http.post('/user/login', {email: email, password: password}).then(function(response) {
			// console.log('response', response)
			if (response.data.token) {
				var token = response.data.token;
				// Set token in HTTP header to authenticate user for future requests
				$http.defaults.headers.common.token = token;
				// Decode token to retrieve user information
				var tokenPayload = jwtHelper.decodeToken(token);
				// console.log('token payload', tokenPayload)
				// redux action to set user information to current state
				self.setCurrentUser(true, token, tokenPayload.id, tokenPayload.email)
				$state.go('home')
			} else {
				$state.go('login')
			}
		})
	}

    return service;
}]);