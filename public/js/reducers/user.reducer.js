var initialState = {
  isAuthenticated: false
};

function user(state = initialState, action) {
  switch (action.type) {
    case 'SET_CURRENT_USER':
      return {
          isAuthenticated: action.isAuthenticated,
          token: action.token,
          _id: action._id,
          email: action.email
      };

    default:
      return state
  }
}
