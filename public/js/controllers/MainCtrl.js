app.controller('MainController', ['$scope', '$ngRedux', '$http', function($scope, $ngRedux, $http) {

  var _this = this;
  var reduxState = $ngRedux.getState();
	console.log('redux store', reduxState);

	_this.mapStateToThis = function(state) {
      return {
        user: state.user
      }
    }
    var unsubscribe = $ngRedux.connect(_this.mapStateToThis, {})(_this)


    $scope.$on('$destroy', unsubscribe);
	

}])