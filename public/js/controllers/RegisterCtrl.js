app.controller('RegisterController', ['$scope', '$state', 'userSvc', function($scope, $state, userSvc) {

	$scope.formSubmit = function() {
		console.log($scope.email, $scope.password)
		userSvc.signUp($scope.email, $scope.password).then(function(data) {
			console.log('then function')
			$state.go('login')
		})
	}	

}]);