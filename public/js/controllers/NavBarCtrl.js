app.controller('NavBarController', ['$scope', '$ngRedux', function($scope, $ngRedux) {
    $ngRedux.subscribe(function() {
        var user = $ngRedux.getState().user;
        console.log('user state from nav bar', user)
        if (user.email) {
            $scope.email = user.email;
        }
    })

    $scope.logout = function() {
        $ngRedux.dispatch(setCurrentUser(false, null, null, null))
        $scope.email = null;
    }

}]);