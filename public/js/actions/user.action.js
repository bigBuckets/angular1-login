// action that sets the user if they login or logout
function setCurrentUser(authenticated, token, _id, email) {
  console.log('set current user action triggered')

  return {
    type: 'SET_CURRENT_USER',
    isAuthenticated: authenticated,
    token: token,
    _id: _id,
    email: email
  };
}