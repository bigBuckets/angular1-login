var app = angular.module('app', [
    'ui.router',
    'appRoutes', 
    'ngRedux',
    'angular-jwt'
]);
// Redux configuration
app.config(function($ngReduxProvider) {
  // redux thunk -> middleware that allows async calls to be made  
  var ReduxThunk = window.ReduxThunk.default;
  // combining all the reducers, if new reducers need to be added, add it here
  var reducers = Redux.combineReducers({ 
    user: user
  })
  // creating the store by passing the reducers and any third party middlewares
  $ngReduxProvider.createStoreWith(reducers, [ReduxThunk])
});

app.run(function($rootScope, $location, $state, $ngRedux) {
  $rootScope.$on('$stateChangeStart', function(event, toState, toParams) {
    var requireLogin = toState.requireLogin;
    // Check if the state requires authentication
    if (requireLogin === true) {
      var reduxState = $ngRedux.getState().user
      var userAuthenticated = reduxState.isAuthenticated
      if (userAuthenticated === false) {
        console.log('Secure route -> redirected to login');
        // if user is not authenticated in the redux store, redirect to login
        event.preventDefault();
        $state.go('login');
      }
    }
  })  

})
