angular.module('appRoutes', []).config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {

	$urlRouterProvider.otherwise('/');

	$stateProvider
		.state('home', {
			templateUrl: 'views/home.html',
			url: '/',
			controller: 'MainController'
		})
		.state('login', {
			templateUrl: 'views/login.html',
			url: '/login',
			controller: 'LoginController'
		})
		.state('register', {
			templateUrl: 'views/register.html',
			url: '/register',
			controller: 'RegisterController',
		})
		.state('auth', {
			templateUrl: 'views/auth.html',
			url: '/auth',
			controller: 'AuthController',
			requireLogin: true,
		})
}]);
