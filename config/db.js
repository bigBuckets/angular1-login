const mongoose = require('mongoose');
mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost:27017/demo');
const db = mongoose.connection;
// mongoose.set('debug', true);
db.on('error', console.error.bind(console, '# Mongo DB: connection error:'));
db.once('open', function() {
    console.log("Mongoose connection created");
})

module.exports = db;